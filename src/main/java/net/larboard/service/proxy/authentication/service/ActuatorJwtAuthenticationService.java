package net.larboard.service.proxy.authentication.service;

import com.netflix.discovery.EurekaClient;
import io.jsonwebtoken.Claims;
import net.larboard.lib.auth.provider.jwt.AJwtAuthenticationService;
import net.larboard.service.proxy.authentication.config.ActuatorAuthConfig;
import net.larboard.service.proxy.authentication.data.AuthUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class ActuatorJwtAuthenticationService extends AJwtAuthenticationService {
    @Autowired
    public ActuatorJwtAuthenticationService(ActuatorAuthConfig actuatorAuthConfig, RestTemplate restTemplate, @Qualifier("eurekaClient") EurekaClient eurekaClient) {
        super(actuatorAuthConfig, restTemplate, eurekaClient);
    }

    @Override
    protected User processUser(Claims claims, String headerToken) {
        return new AuthUser(
                claims.getIssuer(),
                headerToken,
                Collections.singleton(new SimpleGrantedAuthority("ROLE_SYSTEM")
                )
        );
    }
}
