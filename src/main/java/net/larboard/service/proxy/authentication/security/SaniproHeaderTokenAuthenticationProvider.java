package net.larboard.service.proxy.authentication.security;

import net.larboard.lib.auth.security.HeaderTokenAuthenticationProvider;
import net.larboard.lib.auth.service.HeaderTokenAuthenticationService;
import org.springframework.stereotype.Component;

@Component
public class SaniproHeaderTokenAuthenticationProvider extends HeaderTokenAuthenticationProvider {
    public SaniproHeaderTokenAuthenticationProvider(HeaderTokenAuthenticationService authenticationService) {
        super(authenticationService);
    }
}