package net.larboard.service.proxy.authentication.config;

import net.larboard.lib.auth.security.HeaderTokenAuthenticationFilter;
import net.larboard.lib.auth.service.HeaderTokenAuthenticationService;
import net.larboard.service.proxy.authentication.service.ActuatorJwtAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, proxyTargetClass = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .headers().frameOptions().sameOrigin().and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .exceptionHandling().authenticationEntryPoint((req, res, aE) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED))

                .and()
                .authorizeRequests()
                .mvcMatchers(HttpMethod.OPTIONS).permitAll()
                .mvcMatchers(HttpMethod.GET,
                        "/v2/api-docs",
                        "/swagger-resources/**",
                        "/webjars/springfox-swagger-ui/**",
                        "/swagger-ui.html",
                        "/favicon.ico")
                .permitAll()

                .and()
                .authorizeRequests()
                .antMatchers("/actuator/**").hasRole("SYSTEM")

                .and()
                .authorizeRequests()
                .anyRequest().permitAll()//authenticated()

                .and()
                .addFilterBefore(new HeaderTokenAuthenticationFilter(authenticationManagerBean()), UsernamePasswordAuthenticationFilter.class);
    }

    @Configuration
    @Import({HeaderTokenAuthenticationService.class, ActuatorAuthConfig.class})
    public class ConfigureAuthenticationServices {

        @Autowired
        public ConfigureAuthenticationServices(HeaderTokenAuthenticationService headerTokenAuthenticationService,
                                               ActuatorJwtAuthenticationService actuatorJwtAuthenticationService,
                                               ActuatorAuthConfig actuatorAuthConfig) {

            headerTokenAuthenticationService.register(actuatorAuthConfig.getJwtIssuer(), actuatorJwtAuthenticationService);

            headerTokenAuthenticationService.setDefaultProvider(actuatorAuthConfig.getJwtIssuer());
        }
    }
}