package net.larboard.service.proxy.authentication.security;

public class Roles {
    private Roles() {}

    public static final String ROLE_SYSTEM = "ROLE_SYSTEM";
    public static final String ROLE_SYSOP = "ROLE_SYSOP";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_PREMIUM = "ROLE_PREMIUM";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_GUEST = "ROLE_GUEST";
    public static final String ROLE_UNKNOWN = "ROLE_UNKNOWN";
}
